package fr.casden.formation;
import java.lang.System.Logger.Level;

public class Job {

    //@CasdenAuditable(header="Audit de Job, by Me")
    @CasdenAuditable(detail = DetailAudit.STANDARD_RGPD)
    public boolean doTraitement() {
        try{
        System.getLogger(Job.class.getSimpleName()).log(Level.INFO,"Execution de mon traitement");
        Thread.sleep(2000);
        }
        catch(Exception ex) {
            return false;
        }
        return true;
        }

    }
