package fr.casden.formation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import fr.casden.formation.model.*;

public class App {

public static void main(String[] args) throws ProduitIndisponibleException
{
    
    
    HashMap<String,Product> listeBoisson=new HashMap<String,Product>();

    listeBoisson.put("Heineken",new Boisson("Heineken",2.50));
    listeBoisson.put("Evian",new Boisson("Evian",1.50));

    HashMap<String,Product> listeFriandise=new HashMap<String,Product>();

    listeFriandise.put("Heineken",new Friandise("Heineken",2.50));
    listeFriandise.put("Evian",new Friandise("Evian",1.50));

    DistribFriandise distribFriandise=new DistribFriandise(listeFriandise);
    
    DistribBoisson distribBoisson1=new DistribBoisson(listeBoisson);
    DistribBoisson distribBoisson2=new DistribBoisson(listeBoisson);


    distribBoisson1.setTypeLegislation(Legislation.US_SHIELD);

    List<DistribBoisson> ldb=new ArrayList<DistribBoisson>();
    ldb.add(distribBoisson1);
    ldb.add(distribBoisson2);

    for (DistribBoisson distribBoisson:ldb)
    {
        System.out.println(distribBoisson.diagnostic());
    }



    Iterator<DistribBoisson> it=ldb.iterator();
/*
    while (it.hasNext())
    {
        
        System.out.println(l;
        it=(Iterator<DistribBoisson>) it.next();
        
    }
*/
    
    
DistribBoisson distribBoisson=new DistribBoisson(listeBoisson);

    distribBoisson.payBoisson(1);
    distribBoisson.payBoisson(0.2);
    distribBoisson.payBoisson(0.4);


    distribBoisson.choiceBoisson("None");
    distribBoisson.payBoisson(2); 

    distribBoisson.choiceBoisson("Heineken");
    distribBoisson.payBoisson(6);
    

}
}