package fr.casden.formation;

import fr.casden.formation.model.Semaine;
import fr.casden.formation.model.Repas;

public class App3 {
    public static void main(String[] args)
    {
        Semaine jour=Semaine.valueOf("JEUDI");
        System.out.println(jour.name());

        Repas self=Repas.SAMEDI;
        System.out.format("%s c'est %s",self.name(),self.getMenuDuJour());

    }
    
}
