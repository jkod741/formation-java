package fr.casden.formation;

public interface Predicate<T> {
    public boolean accept(T t);
}
