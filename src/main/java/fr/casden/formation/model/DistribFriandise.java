package fr.casden.formation.model;

import java.util.Hashtable;
import java.util.Map;

public class DistribFriandise extends Distrib {
    private Product friandise=null;
    //private Hashtable<String,Boisson> listeBoisson=new Hashtable<String,Boisson>();
    //listeBoisson=new Hashtable<String,Boisson>();

    private Map<String,Product> listeFriandise;

    private double montantIntroduit=0;

    public void choiceFriandise(String choix) throws ProduitIndisponibleException
    {
       
        /*
        if (listeBoisson.containsKey(choix))
        {
            boisson=listeBoisson.get(choix);
            System.out.println(choix+" accepté");
        }
        else
        {
            System.out.println(choix+" pas accepté");
        }
    }
    */


friandise=listeFriandise.get(choix);
        System.out.println(choix+" accepté"); 







}
        
    public void payFriandise(double montant)
    {
        try
        {
        montantIntroduit+=montant;

        if (montantIntroduit<friandise.getPrice())
        {
            System.out.println("Montant insuffisant - rajouté "+(friandise.getPrice()-montantIntroduit));
        }
        else if (montantIntroduit==friandise.getPrice())
        {
            System.out.println("Paiement effectué");
            montantIntroduit=0;
            distribuerFriandise(friandise);
            friandise=null;
        }
        else {
            distribuerFriandise(friandise);
            rendreMonnaie(montantIntroduit);
            friandise=null;
        }
    }
    catch (Exception ex)
    {
        System.out.println("PayFriandise exception");
    } 
    } 

    private void rendreMonnaie(double montant)
    {
        System.out.println("Récupére votre monnaie: "+(montant-friandise.getPrice()));
        montantIntroduit=0;
    }

    public DistribFriandise(Map<String,Product> listeFriandise)
    {
        super(listeFriandise);
        this.listeFriandise=listeFriandise;
    }

    private void distribuerFriandise(Product friandise)
    {
        //System.out.printf("Distribution de %f à un tarif de %d ", boisson.getLabel(), boisson.getPrice());
        System.out.println("Retirer friandise "+friandise.getLabel()+" au prix de: "+friandise.getPrice()+" EUR");
    }

    public double getFriandisePrice()
    {
        return friandise.getPrice();
    }

    public Product getFriandise() {
        return friandise;
    }

    public boolean isChargeable()
    {
        return false;
    }   
    public String diagnostic()
    {
        return "Diagnostic module";
    }   
}





