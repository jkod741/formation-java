package fr.casden.formation.model;

public class Boisson extends Product {
    private String label;
    private double price;

    public Boisson(String label, double price)
    {
        super(label,price);
        this.label=label;
        this.price=price;

    }
   
    public String getLabel()
    {
        return label;
    }

    public double getPrice()
    {
        return price;
    }

}
