package fr.casden.formation.model;

public class ProduitIndisponibleException extends Exception{

    public ProduitIndisponibleException() {
    }

    public ProduitIndisponibleException(String message) {
        super(message);
    }

    public ProduitIndisponibleException(Throwable cause) {
        super(cause);
    }

    public ProduitIndisponibleException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProduitIndisponibleException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }  
    
}
