package fr.casden.formation.model;

public enum Repas{
    LUNDI("Pizza"),
    MARDI("Choucroute"),
    MERCREDI("Raclette"),
    JEUDI("Kebab"),
    VENDREDI("Saumon"),
    SAMEDI("Couscous"),
    DIMANCHE("Hamburger");

    private String menuDuJour;
    private Repas(String menuDuJour)
    {
        this.menuDuJour=menuDuJour;
    }
    public String getMenuDuJour() {
        return menuDuJour;
    }
    
}

