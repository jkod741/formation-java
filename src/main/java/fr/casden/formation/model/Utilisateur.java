package fr.casden.formation.model;

public class Utilisateur {
    private String nom;
    private String prenom;
    private int age;

    public Utilisateur(String nom, String prenom, int age)
    {
        this.nom=nom;
        this.prenom=prenom;
        this.age=age;
    }

    public String getNom()
    {
        return nom.toUpperCase();
    }
    
    /**
     * 
     * @return Empty
     */

    public String getPrenom()
    {
        return prenom;
    }

    public int getAge()
    {
        return age;
    }

/*    
    public void setAge(int age)
    {
        this.age=age;    
    }
*/

public int vieillir(int nbannees)
{
    if (nbannees<=0) return age;
    if (age+nbannees>100)
    {
        age=100;
        return age;
    }
    age=age+nbannees;
    return age;
}

public String toString()
{
    return nom+" "+prenom+" "+age;
}

}
