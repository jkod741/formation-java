package fr.casden.formation.model;

public enum Legislation {
    
    DEFAULT,
    RGPD,
    US_SHIELD

}
