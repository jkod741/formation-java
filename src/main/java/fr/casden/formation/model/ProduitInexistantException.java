package fr.casden.formation.model;

public class ProduitInexistantException extends RuntimeException {

    public ProduitInexistantException() {
    }

    public ProduitInexistantException(String message) {
        super(message);
    }

    public ProduitInexistantException(Throwable cause) {
        super(cause);
    }

    public ProduitInexistantException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProduitInexistantException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
