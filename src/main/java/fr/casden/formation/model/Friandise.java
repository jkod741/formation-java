package fr.casden.formation.model;

public class Friandise extends Product {
    private String label;
    private double price;

    public Friandise(String label, double price)
    {
        super(label,price);
        this.label=label;
        this.price=price;

    }
   
    public String getLabel()
    {
        return label;
    }

    public double getPrice()
    {
        return price;
    }

}
