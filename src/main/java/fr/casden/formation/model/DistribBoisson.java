package fr.casden.formation.model;

import java.util.Hashtable;
import java.util.Map;

public class DistribBoisson extends Distrib {
    private Product boisson=null;
    //private Hashtable<String,Boisson> listeBoisson=new Hashtable<String,Boisson>();
    //listeBoisson=new Hashtable<String,Boisson>();

    private Map<String,Product> listeBoisson;

    private double montantIntroduit=0;

    public void choiceBoisson(String choix) throws ProduitIndisponibleException
    {
       
        
        if (listeBoisson.containsKey(choix))
        {
            boisson=listeBoisson.get(choix);
            System.out.println(choix+" accepté");
        }
        else
        {
            System.out.println(choix+" pas accepté");
            throw new ProduitIndisponibleException("Detect ProIndExc");
        }
    }








        
    public void payBoisson(double montant)
    {
        try
        {
        montantIntroduit+=montant;

        if (montantIntroduit<boisson.getPrice())
        {
            System.out.println("Montant insuffisant - rajouté "+(boisson.getPrice()-montantIntroduit));
        }
        else if (montantIntroduit==boisson.getPrice())
        {
            System.out.println("Paiement effectué");
            montantIntroduit=0;
            distribuerBoisson(boisson);
            boisson=null;
        }
        else {
            distribuerBoisson(boisson);
            rendreMonnaie(montantIntroduit);
            boisson=null;
        }
    }
    catch (Exception ex)
    {
        System.out.println("PayBoisson exception");
    } 
    } 

    private void rendreMonnaie(double montant)
    {
        System.out.println("Récupére votre monnaie: "+(montant-boisson.getPrice()));
        montantIntroduit=0;
    }

    public DistribBoisson(Map<String,Product> listeBoisson)
    {
        super(listeBoisson);
        this.listeBoisson=listeBoisson;
    }

    private void distribuerBoisson(Product boisson)
    {
        //System.out.printf("Distribution de %f à un tarif de %d ", boisson.getLabel(), boisson.getPrice());
        System.out.println("Retirer boisson "+boisson.getLabel()+" au prix de: "+boisson.getPrice()+" EUR");
    }

    public double getBoissonPrice()
    {
        return boisson.getPrice();
    }

    public Product getBoisson() {
        return boisson;
    }

    public boolean isChargeable()
    {
        return false;
    }   
    public String diagnostic()
    {
        return "Diagnostic module";
    }   
}





