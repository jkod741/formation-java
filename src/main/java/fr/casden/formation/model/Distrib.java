package fr.casden.formation.model;

import java.util.Hashtable;
import java.util.Map;

public abstract class Distrib {
    private Product product=null;
    //private Hashtable<String,Boisson> listeBoisson=new Hashtable<String,Boisson>();
    //listeBoisson=new Hashtable<String,Boisson>();

    private Legislation typeLegislation=Legislation.DEFAULT;
    private Map<String,Product> listProduct;

    private double montantIntroduit=0;

    public void setTypeLegislation(Legislation typeLegislation)
    {
        this.typeLegislation=typeLegislation;
    }

    public void choiceProduct(String choix)
    {
        if (listProduct.containsKey(choix))
        {
            product=listProduct.get(choix);
            System.out.println(choix+" accepté");
        }
        else
        {
            System.out.println(choix+" pas accepté");
        }
    }
        
    public void payProduct(double montant)
    {
        montantIntroduit+=montant;
        if (product==null)
        {
            System.out.println("merci d'abord de selectionner le produit");
        }
        if (montantIntroduit<product.getPrice())
        {
            System.out.println("Montant insuffisant - rajouté "+(product.getPrice()-montantIntroduit));
        }
        else if (montantIntroduit==product.getPrice())
        {
            System.out.println("Paiement effectué");
            montantIntroduit=0;
            distribuerProduct(product);
        }
        else {
            distribuerProduct(product);
            rendreMonnaie(montantIntroduit);
        }

    }

    private void rendreMonnaie(double montant)
    {
        System.out.println("Récupére votre monnaie: "+(montant-product.getPrice()));
        montantIntroduit=0;
    }

    public Distrib(Map<String,Product> listProduct)
    {
        this.listProduct=listProduct;
    }

    private void distribuerProduct(Product product)
    {
        //System.out.printf("Distribution de %f à un tarif de %d ", boisson.getLabel(), boisson.getPrice());
        System.out.println("Retirer produit "+product.getLabel()+" au prix de: "+product.getPrice()+" EUR");
        enregistrerTransaction();
    }

    public double getProductPrice()
    {
        return product.getPrice();
    }

    public Product getProduct() {
        return product;
    }


    private void enregistrerTransaction()
    {
        System.out.println("Enregistrement transaction");
        switch(typeLegislation){
            case DEFAULT:
              System.out.println("Enregistrement local");
              break;
            case RGPD:
              System.out.println("Pas Enregistrement");
              break;
            case US_SHIELD:
            System.out.println("Enregistrement aux US");
            break;


        }
    }

    public abstract boolean isChargeable();
    public abstract String diagnostic();
   

}





