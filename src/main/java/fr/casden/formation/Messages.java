package fr.casden.formation;

import java.util.Locale;
import java.util.ResourceBundle;

public class Messages {
   private Messages() {}
   private static final String BUNDLE_NAME="fr.casden.formation.messages";
   private static final ResourceBundle RESOURCE_BUNDLE=ResourceBundle.getBundle(BUNDLE_NAME);

   public static String getValue(String key)
   {
       return RESOURCE_BUNDLE.getString(key);
   }
   
   public static String getValue(String key,Locale locale)
   {
       return ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(key);
   }
}
