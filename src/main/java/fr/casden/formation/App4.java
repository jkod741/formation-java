package fr.casden.formation;

import fr.casden.formation.model.Connection;

public class App4 {
    public static void main(String[] args)
    {
         Connection cnx1=Connection.getInstance();
         Connection cnx2=Connection.getInstance();
         System.out.println(cnx1==cnx2?"Identique":"Different");
         System.out.println(cnx1.toString());
         System.out.println(cnx2.toString());
    }
    
}

