package fr.casden.formation;

public class App8 {

    public static void main(String[] args)
    {
        System.out.println("Begin");
        Thread t1=new Thread(new Ascenseur("Asc1",Math.round(4.0*Math.random())));
        Thread t2=new Thread(new Ascenseur("Asc2",Math.round(4.0*Math.random())));
         
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        System.out.println("End");
    }
    
}
