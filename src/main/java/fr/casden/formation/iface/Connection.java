package fr.casden.formation.iface;

public interface Connection {
    public void  executeQuery(String sql);
}
