package fr.casden.formation;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class App15 {

    public static void afficheDateTime() {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }


    public static void main(String[] args) throws Exception{
        var job=new Job();

        var auditable=Job.class.getDeclaredMethod("doTraitement").getAnnotation(CasdenAuditable.class);

        var isAuditable=(auditable!=null);

        if (isAuditable) {
            System.out.println(auditable.header());
            switch (auditable.detail()){
                case DETAILLE:
                case STANDARD:
                      System.out.println("Audit sans prise en compte RGPD");
                      break;
                case DETAILLE_RGPD:
                case STANDARD_RGPD:
                      System.out.println("Audit avec prise en compte RGPD");
            }
            afficheDateTime();
        }
        
       // afficheDateTime();
        job.doTraitement();

        if (isAuditable) {
            afficheDateTime();
        }
        
    }
    
}
