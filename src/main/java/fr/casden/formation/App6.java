package fr.casden.formation;

import java.util.Locale;

public class App6 {
    public static void main(String[] args)
    {
        Locale.setDefault(Locale.FRANCE);
        String nom=Messages.getValue("nom");
        String prenom=Messages.getValue("prenom");
        String date=Messages.getValue("date");

        System.out.format("%s %s %s\n",prenom,nom,date);
        System.out.println(Locale.getDefault());

        System.out.println(Messages.getValue("prenom",Locale.ENGLISH));
        
    }
  
}
