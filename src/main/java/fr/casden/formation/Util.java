package fr.casden.formation;

import java.util.Comparator;
import java.util.List;

public class Util <T>{
    public static <T> void customSort(List<T> list, Comparator<T> comparator)
    {
        list.sort(comparator);
    }

    public T recherche(List<T> list, Predicate<T> predicate)
    {

        for(T t:list)
        {
            if (predicate.accept(t))
            return t;
        }
        return null;
    }

    
}
