package fr.casden.formation;
import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CasdenAuditable {
    String header() default "Audit en cours";
    DetailAudit detail() default DetailAudit.STANDARD;
}
